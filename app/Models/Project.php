<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use App\Models\User;

class Project extends Model
{
    use HasFactory;

    protected $table = 'projects';
    public $timestamps = true;

    protected $fillable = [
        'name',
        'description',
        'manager',
        'assigned',
        'status',
        'created_at',
        'updated_at'
    ];

    public function getManager()
    {
        return $this->hasOne(User::class, 'id', 'manager');
	}

    public function getAssigned()
    {
        return $this->hasOne(User::class, 'id', 'assigned');
    }
}