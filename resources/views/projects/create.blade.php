<x-app-layout>
    <x-slot name="header">
        <h2 class="flex font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Create project') }}
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-6 bg-white border-b border-gray-200">
                    
                    @if ($errors->any())
                        <div class="p-3 text-white bg-red-600">
                            <strong>Whoops!</strong> There were some problems with your input.<br><br>
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                    <form action="{{ route('projects.store') }}" method="POST" >
                        @csrf

                        <strong>Name:</strong><br>
                        <input type="text" name="name" class="w-full mb-3" placeholder="Name" required><br>
            
                        <strong>Description:</strong><br>
                        <input type="text" name="description" class="w-full mb-3" placeholder="Description" required><br>
                    
                        <strong>Project manager:</strong><br>
                        <select name="manager" class="w-full mb-3" required>
                            @foreach ($users as $item)
                                <option value="{{ $item->id }}">{{ $item->name }}</option>
                            @endforeach
                        </select><br>

                        <strong>Assigned to:</strong><br>
                        <select name="assigned" class="w-full mb-3" required>
                            @foreach ($users as $item)
                                <option value="{{ $item->id }}">{{ $item->name }}</option>
                            @endforeach
                        </select><br>

                        <strong>Status:</strong><br>
                        <select name="status" class="w-full mb-3" required>
                            <option value="enabled">Enabled</option>
                            <option value="disabled">Disabled</option>
                        </select><br>

                        <button type="submit" class="bg-red-600 text-white p-3 rounded">Create project</button>

                    </form>
                </div>
            </div>
        </div>
    </div>
</x-app-layout>