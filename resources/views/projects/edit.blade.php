<x-app-layout>
    <x-slot name="header">
        <h2 class="flex font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Edit project') }}
        </h2>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-6 bg-white border-b border-gray-200">
                    
                    @if ($errors->any())
                        <div class="p-3 text-white bg-red-600">
                            <strong>Whoops!</strong> There were some problems with your input.<br><br>
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif

                    <form action="{{ route('projects.update', $project->id) }}" method="POST">
                        @csrf
                        @method('PUT')

                        <strong>Name:</strong><br>
                        <input type="text" name="name" class="w-full mb-3" placeholder="Name" value="{{ $project->name }}"><br>
            
                        <strong>Description:</strong><br>
                        <input type="text" name="description" class="w-full mb-3" placeholder="Description" value="{{ $project->description }}"><br>
                    
                        <strong>Project manager:</strong><br>
                        <select name="status" class="w-full mb-3">
                            @foreach ($users as $item)
                                <option value="{{ $item->id }}" @if ($project->manager == $item->id) selected="selected" @endif>{{ $item->name }}</option>
                            @endforeach
                        </select><br>

                        <strong>Assigned to:</strong><br>
                        <select name="status" class="w-full mb-3">
                            @foreach ($users as $item)
                                <option value="{{ $item->id }}" @if ($project->assigned == $item->id) selected="selected" @endif>{{ $item->name }}</option>
                            @endforeach
                        </select><br>

                        <strong>Status:</strong><br>
                        <select name="status" class="w-full mb-3">
                            <option value="enabled" @if ($project->status == 'enabled') selected="selected" @endif>Enabled</option>
                            <option value="disabled" @if ($project->status == 'disabled') selected="selected" @endif>Disabled</option>
                        </select><br>

                        <button type="submit" class="bg-red-600 text-white p-3 rounded">Save changes</button>

                    </form>
                </div>
            </div>
        </div>
    </div>
</x-app-layout>