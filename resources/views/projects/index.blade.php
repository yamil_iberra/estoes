<x-app-layout>
    <x-slot name="header">
        <div class="flex items-center content-between">
            <h2 class="font-semibold text-xl text-gray-800 leading-tight">{{ __('My projects') }}</h2>
            <a href="{{ route('projects.create') }}" class="p-2 rounded bg-red-600 text-white ml-6">+ Add project</a>
        </div>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-6 bg-white border-b border-gray-200">
                    
                    @if ($message = Session::get('success'))
                        <div class="p-3 text-white bg-green-600">
                            <p>{{ $message }}</p>
                        </div>
                    @endif

                    <table class="w-full table-auto">
                        <tr>
                            <th class="text-left">Project info</th>
                            <th class="text-left">Project Manager</th>
                            <th class="text-left">Assigned to</th>
                            <th class="text-left">Status</th>
                            <th class="text-right" width="280px">Action</th>
                        </tr>
                        @foreach ($projects as $project)

                            <tr>
                                <td>
                                    {{ $project->name }}<br>
                                    <small>{{ $project->description }}</small>
                                </td>
                                <td>
                                    {{ $project->getManager->name }}
                                </td>
                                <td>
                                    {{ $project->getAssigned->name }}
                                </td>
                                <td>
                                    <div class="p-2 bg-gray-300">{{ $project->status }}</div>
                                </td>
                                <td class="text-right">

                                    <a href="{{ route('projects.edit', $project->id) }}">
                                        <i class="fas fa-edit fa-lg"></i>
                                    </a>

                                    <form action="{{ route('projects.destroy', $project->id) }}" method="POST">
                                        @csrf
                                        @method('DELETE')

                                        <button type="submit" title="delete" style="border: none; background-color:transparent;">
                                            <i class="fas fa-trash fa-lg text-danger"></i>
                                        </button>
                                    </form>
                                </td>
                            </tr>
                            
                        @endforeach
                    </table>

                    {!! $projects->links() !!}
                </div>
            </div>
        </div>
    </div>
</x-app-layout>