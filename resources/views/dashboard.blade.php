<x-app-layout>
    <x-slot name="header">
        <h2 class="flex font-semibold text-xl text-gray-800 leading-tight">
            {{ __('My Projects') }}
        </h2>

        <div class="flex sm:ml-6">
            Add project
        </div>
    </x-slot>

    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                <div class="p-6 bg-white border-b border-gray-200">
                    @if ($message = Session::get('success'))
                        <div class="alert alert-success">
                            <p>{{ $message }}</p>
                        </div>
                    @endif

                    <table class="table table-bordered table-responsive-lg">
                        <tr>
                            <th>No</th>
                            <th>Name</th>
                            <th>Introduction</th>
                            <th>Location</th>
                            <th>Cost</th>
                            <th>Date Created</th>
                            <th width="280px">Action</th>
                        </tr>
                        @foreach ($projects as $project)
                            <tr>
                                <td>{{ ++$i }}</td>
                                <td>{{ $project->name }}</td>
                                <td>{{ $project->introduction }}</td>
                                <td>{{ $project->location }}</td>
                                <td>{{ $project->cost }}</td>
                                <td>{{ date_format($project->created_at, 'jS M Y') }}</td>
                                <td>
                                    <form action="{{ route('projects.destroy', $project->id) }}" method="POST">

                                        <a href="{{ route('projects.show', $project->id) }}" title="show">
                                            <i class="fas fa-eye text-success  fa-lg"></i>
                                        </a>

                                        <a href="{{ route('projects.edit', $project->id) }}">
                                            <i class="fas fa-edit  fa-lg"></i>

                                        </a>

                                        @csrf
                                        @method('DELETE')

                                        <button type="submit" title="delete" style="border: none; background-color:transparent;">
                                            <i class="fas fa-trash fa-lg text-danger"></i>

                                        </button>
                                    </form>
                                </td>
                            </tr>
                        @endforeach
                    </table>

                    {!! $projects->links() !!}
                </div>
            </div>
        </div>
    </div>
</x-app-layout>
